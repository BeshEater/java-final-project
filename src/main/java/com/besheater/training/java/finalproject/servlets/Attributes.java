package com.besheater.training.java.finalproject.servlets;

public class Attributes {
    public static final String LOCALE = "localeFromUrlPrefix";
    public static final String LOGGED_USER = "loggedUser";
    public static final String USER = "user";
    public static final String PROJECT = "project";
    public static final String PROJECTS = "projects";
    public static final String ERROR = "error";
    public static final String PAGE_URL = "pageUrl";
}
