 $.validator.setDefaults( {
        submitHandler: function () {
            alert( "submitted!" );
        }
    } );

$( document ).ready( function () {
    $( "#add-project-form" ).click(function() {
        $(".error-block").fadeOut();
    });

    $( "#add-project-form" ).validate( {
        rules: {
            project_name: {
                required: true,
                alphanumeric: true,
                rangelength: [1, 50],
            },
            description: {
                rangelength: [1, 500],
            },
            ocr_wc_rating_threshold: {
                required: true,
                number: true,
                range: [0, 1.0],
            },
            verifications_per_word: {
                required: true,
                digits: true,
                range: [1, 10],
            },
        },
        messages: {
            project_name: {
                required: "Please enter project name",
                alphanumeric: "Must consist of only letters, numbers, hyphens and underscores",
                rangelength: "Length must be between 1 and 50 characters",
            },
            description: {
                rangelength: "Length must be between 1 and 500 characters",
            },
            ocr_wc_rating_threshold: {
                required: "Please enter OCR Word Confidence rating threshold",
                number: "Please enter number",
                range: "Value must be between 0 and 1.0",
            },
            verifications_per_word: {
                required: "Please enter Verification per word",
                digits: "Please enter integer number",
                range: "Value must be between 1 and 10",
            },
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "form-validation-hint-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        }
    } );
} );