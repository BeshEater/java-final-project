<%@ page import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<template:main pageUrl="${requestScope[Attributes.PAGE_URL]}">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.user.title"/>
    </jsp:attribute>
    <jsp:body>
        <main id="main">

            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.user.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.user.title"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->

            <!-- ======= User Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <c:if test="${requestScope[Attributes.ERROR] != null}">
                                <div class="general-block error-block">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-2">
                                            <i class="icofont-close-circled"></i>
                                        </div>
                                        <div class="col-lg-10">
                                            <p>
                                                <fmt:message key='page.user.error.user-not-exist'>
                                                    <fmt:param>${fn:escapeXml(param.username)}</fmt:param>
                                                </fmt:message>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <c:if test="${requestScope[Attributes.USER] != null}">
                                <c:set var = "user" value = "${requestScope[Attributes.USER]}"/>
                                <div class="user-box">
                                    <h3 class="text-center">${fn:escapeXml(user.username)}</h3>
                                    <div class="row">
                                        <p class="col-lg-6"><fmt:message key="page.sign-up.form.first-name"/></p>
                                        <p class="col-lg-6">${fn:escapeXml(user.firstName)}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-6"><fmt:message key="page.sign-up.form.last-name"/></p>
                                        <p class="col-lg-6">${fn:escapeXml(user.lastName)}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-6"><fmt:message key="page.sign-up.form.email"/></p>
                                        <p class="col-lg-6">${fn:escapeXml(user.email)}</p>
                                    </div>
                                    <div class="row">
                                        <p class="col-lg-6"><fmt:message key="page.user.member-since"/></p>
                                        <p class="col-lg-6"><javatime:format value="${user.registrationTime}" style="MS" /></p>
                                    </div>
                                </div>
                            </c:if>
                        </div>
                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->
    </jsp:body>
</template:main>