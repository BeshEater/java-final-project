$( document ).ready( function () {
    $( "#sign-up-form" ).click(function() {
        $(".error-block").fadeOut();
    });

    $( "#sign-up-form" ).validate( {
        rules: {
            first_name: {
                required: true,
                alphanumeric: true,
                rangelength: [1, 50],
            },
            last_name: {
                required: true,
                alphanumeric: true,
                rangelength: [1, 100],
            },
            username: {
                required: true,
                alphanumeric: true,
                rangelength: [3, 20],
            },
            password: {
                required: true,
                rangelength: [6, 100],
            },
            confirm_password: {
                required: true,
                rangelength: [6, 100],
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: $.validator.localisedMessages,
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "form-validation-hint-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        }
    } );
} );