package com.besheater.training.java.finalproject.services;

import com.besheater.training.java.finalproject.entities.User;
import com.besheater.training.java.finalproject.repositories.UserRepo;
import com.besheater.training.java.finalproject.services.exceptions.UserAlreadyExistException;
import com.besheater.training.java.finalproject.services.exceptions.UserNotExistException;
import com.besheater.training.java.finalproject.services.exceptions.WrongCredentialsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

public class UserManager {
    private static final Logger LOG = LogManager.getLogger();

    private final UserRepo userRepo;
    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

    public UserManager(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    public User authenticate(String username, String rawPassword) throws WrongCredentialsException,
            UserNotExistException {
        LOG.debug("Authenticating user with username {}", username);
        Optional<User> optionalUser = userRepo.findOneByUsername(username);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            boolean isPasswordCorrect = encoder.matches(rawPassword, user.getEncodedPassword());
            if (isPasswordCorrect) {
                return user;
            } else {
                throw new WrongCredentialsException("User password is wrong");
            }
        } else {
            throw new UserNotExistException("User with username " + username + " doesn't exits");
        }
    }

    public static String encodePassword(String rawPassword) {
        return encoder.encode(rawPassword);
    }

    public Optional<User> registerUser(User user) throws UserAlreadyExistException {
        LOG.debug("Registering user: {}", user);
        String username = user.getUsername();
        String email = user.getEmail();
        if (userRepo.findOneByUsername(username).isPresent()) {
            throw new UserAlreadyExistException("User with username " + username +" already exist");
        }
        if (userRepo.findOneByEmail(email).isPresent()) {
            throw new UserAlreadyExistException("User with email " + email + " already exist");
        }
        return userRepo.save(user);
    }

    public Optional<User> getUserByUsername(String username) {
        if (username != null && !username.equals("")) {
            return userRepo.findOneByUsername(username);
        }
        return Optional.empty();
    }
}