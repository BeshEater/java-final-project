/* Users */
INSERT INTO users(username, first_name, last_name, email, password, registration_time) VALUES (
'JoCAR', 'Jack', 'Carter', 'jack_c@mail.ru','$2a$10$6BHI97yqbpMFzZ7yMjtkAeVxMpF9xMOULzORsWMqa2MHjVHOCmY.a', TIMESTAMP '2019-12-30 22:59:12'
);

INSERT INTO users(username, first_name, last_name, email, password, registration_time) VALUES (
'T_MKS', 'Mike', 'Simpson', 'simp.timp@gmail.com', '$2a$10$wEDmalPrOwgRBuBKKxKm/.9IA5z6WOvph90IcNQ6jgwMfaAGFXF5a', TIMESTAMP '2020-02-19 15:00:06'
);

INSERT INTO users(username, first_name, last_name, email, password, registration_time) VALUES (
'Ив1337', 'Иван', 'Васильевич', 'ivan.pro@mail.ru','$2a$10$/3oggfEPkWgBlE84zkK8zOfgVsLWjrX4U0YRon7W55qREpBbu1KR.', TIMESTAMP '2020-03-25 14:00:54'
);


/* Projects */
INSERT INTO projects(name, description, ocr_wc_rating_threshold, verifications_per_word, user_id, creation_time) VALUES (
'Astra', 'Newspapers archive from Astra library', 0.85, 3, 1, TIMESTAMP '2019-12-31 06:03:42'
);

INSERT INTO projects(name, description, ocr_wc_rating_threshold, verifications_per_word, user_id, creation_time) VALUES (
'Retro-sci-fi', 'Old sci-fi journals', 0.9, 4, 1, TIMESTAMP '2019-12-31 15:03:00'
);

INSERT INTO projects(name, description, ocr_wc_rating_threshold, verifications_per_word, user_id, creation_time) VALUES (
'Project-red', 'Archive of games related text documents', 0.87, 3, 2, TIMESTAMP '2020-03-01 18:32:34'
);

INSERT INTO projects(name, description, ocr_wc_rating_threshold, verifications_per_word, user_id, creation_time) VALUES (
'Кедр', 'Архивные записи военных документов холодной войны', 0.91, 3, 3, TIMESTAMP '2020-03-25 20:18:01'
);

INSERT INTO projects(name, description, ocr_wc_rating_threshold, verifications_per_word, user_id, creation_time) VALUES (
'Знание-88', 'Малотиражные книги по технической тематике', 0.89, 5, 3, TIMESTAMP '2020-03-26 07:00:09'
);