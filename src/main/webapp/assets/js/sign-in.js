$( document ).ready( function () {
    $( "#sign-in-form" ).click(function() {
        $(".error-block").fadeOut();
    });

    $( "#sign-in-form" ).validate( {
        rules: {
            username: "required",
            password: "required",
        },
        messages: $.validator.localisedMessages,
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "form-validation-hint-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).removeClass( "is-invalid" );
        }
    } );
} );