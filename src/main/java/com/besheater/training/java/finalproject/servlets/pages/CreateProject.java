package com.besheater.training.java.finalproject.servlets.pages;

import com.besheater.training.java.finalproject.entities.Project;
import com.besheater.training.java.finalproject.entities.User;
import com.besheater.training.java.finalproject.filters.LocaleDispatcher;
import com.besheater.training.java.finalproject.services.DiContainer;
import com.besheater.training.java.finalproject.services.ProjectManager;
import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

@WebServlet("/create-project")
public class CreateProject extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger();

    private ProjectManager projectManager;

    @Override
    public void init() throws ServletException {
        projectManager = DiContainer.getInstance().getProjectManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("Get request");
        dispatchToView(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("POST request");
        User user = (User) req.getSession().getAttribute(Attributes.LOGGED_USER);
        String projectName = req.getParameter("project_name");
        String description = req.getParameter("description");
        Double ocrWcRatingThreshold = null;
        Integer verificationsPerWord = null;
        try {
            ocrWcRatingThreshold = Double.valueOf(req.getParameter("ocr_wc_rating_threshold"));
            verificationsPerWord = Integer.valueOf(req.getParameter("verifications_per_word"));
        } catch (NumberFormatException ex) {
            LOG.error("Malformed numbers string", ex);
        }

        if (user != null && projectName != null &&
                ocrWcRatingThreshold != null && verificationsPerWord != null) {
            try {
                Project project = new Project.Builder()
                        .setUser(user)
                        .setName(projectName)
                        .setDescription(description)
                        .setOcrWcRatingThreshold(ocrWcRatingThreshold)
                        .setVerificationPerWord(verificationsPerWord)
                        .setCreationTime(Instant.now())
                        .build();
                Optional<Project> savedProject = projectManager.saveProject(project);
                if (savedProject.isPresent()) {
                    LocaleDispatcher.redirect(req, resp, "/projects");
                    LOG.debug("New project successfully created {}", savedProject.get());
                } else {
                    LOG.error("Can't save project {}", project);
                    req.setAttribute(Attributes.ERROR, "general.form.error.data-is-not-valid");
                    dispatchToView(req, resp);
                }
            } catch (IllegalArgumentException ex) {
                LOG.error("Project is not valid", ex);
                req.setAttribute(Attributes.ERROR, "general.form.error.data-is-not-valid");
                dispatchToView(req, resp);
            }
        } else {
            LOG.error("Some fields are null");
            req.setAttribute(Attributes.ERROR, "page.sign-up.error.data-is-not-valid");
            dispatchToView(req, resp);
        }
    }

    private void dispatchToView(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/view/create-project.jsp").forward(req, resp);
    }
}