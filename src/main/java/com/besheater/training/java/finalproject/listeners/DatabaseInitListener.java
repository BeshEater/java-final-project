package com.besheater.training.java.finalproject.listeners;

import com.besheater.training.java.finalproject.entities.User;
import com.besheater.training.java.finalproject.services.DiContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.h2.tools.RunScript;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;

@WebListener
public class DatabaseInitListener implements ServletContextListener {
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        initDatabase();
    }

    private void initDatabase() {
        LOG.debug("Database initialisation start");
        DataSource dataSource = DiContainer.getInstance().getDataSource();
        try {
            Connection connection = dataSource.getConnection();
            RunScript.execute(connection, getFile("/schema.sql"));
            RunScript.execute(connection, getFile("/data.sql"));
            LOG.debug("Database initial tables and data created.");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private Reader getFile(String resourcePath) {
        return new InputStreamReader(DatabaseInitListener.class.getResourceAsStream(resourcePath),
                StandardCharsets.UTF_8);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}