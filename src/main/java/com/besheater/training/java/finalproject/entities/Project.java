package com.besheater.training.java.finalproject.entities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.Objects;
import java.util.regex.Pattern;

public class Project {
    private static final Logger LOG = LogManager.getLogger();

    private static final String NAME_REGEX = "[\\w-]{1,50}";
    private static final int MIN_DESCRIPTION_LENGTH = 1;
    private static final int MAX_DESCRIPTION_LENGTH = 500;
    private static final double MIN_OCR_WC_RATING_THRESHOLD = 0;
    private static final double MAX_OCR_WC_RATING_THRESHOLD = 1.0;
    private static final int MIN_VERIFICATIONS_PER_WORD = 1;
    private static final int MAX_VERIFICATIONS_PER_WORD = 10;

    private final Long id;
    private final String name;
    private final String description;
    private final double ocrWcRatingThreshold;
    private final int verificationsPerWord;
    private final User user;
    private final Instant creationTime;

    private Project(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.ocrWcRatingThreshold = builder.ocrWcRatingThreshold;
        this.verificationsPerWord = builder.verificationsPerWord;
        this.user = builder.user;
        this.creationTime = builder.creationTime;
    }

    private static void validateId(Long id) {
        if(id == null || id < 0) {
            throw new IllegalArgumentException("Id: " + id + " is not valid");
        }
    }

    private static void validateName(String name) {
        if(name == null || !isMatch(name, NAME_REGEX)) {
            throw new IllegalArgumentException("Name: " + name + " is not valid");
        }
    }

    private static void validateDescription(String description) {
        if (description != null && !description.equals("")) {
            if (description.length() < MIN_DESCRIPTION_LENGTH ||
                    description.length() > MAX_DESCRIPTION_LENGTH) {
                throw new IllegalArgumentException("Description: " + description + " is not valid");
            }
        }
    }

    private static void validateOcrWcRatingThreshold(Double ocrWcRatingThreshold) {
        if (ocrWcRatingThreshold == null || ocrWcRatingThreshold < MIN_OCR_WC_RATING_THRESHOLD
                || ocrWcRatingThreshold > MAX_OCR_WC_RATING_THRESHOLD) {
            throw new IllegalArgumentException("OCR WC rating threshold: " + ocrWcRatingThreshold + " is not valid");
        }
    }

    private static void validateVerificationsPerWord(Integer verificationsPerWord) {
        if (verificationsPerWord == null || verificationsPerWord < MIN_VERIFICATIONS_PER_WORD
                || verificationsPerWord > MAX_VERIFICATIONS_PER_WORD) {
            throw new IllegalArgumentException("Number of verifications per word: " + verificationsPerWord + " is not valid");
        }
    }

    private static void validateUser(User user) {
        if (user == null || user.getId() == null) {
            throw new IllegalArgumentException("User: " + user + " is not valid");
        }
    }

    private static void validateCreationTime(Instant instant) {
        if (instant == null) {
            throw new IllegalArgumentException("Creation time: " + instant + " is not valid");
        }
    }

    private static boolean isMatch(CharSequence input, String regex) {
        Pattern p = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        return p.matcher(input).matches();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getOcrWcRatingThreshold() {
        return ocrWcRatingThreshold;
    }

    public int getVerificationsPerWord() {
        return verificationsPerWord;
    }

    public User getUser() {
        return user;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Double.compare(project.ocrWcRatingThreshold, ocrWcRatingThreshold) == 0 &&
                verificationsPerWord == project.verificationsPerWord &&
                Objects.equals(id, project.id) &&
                name.equals(project.name) &&
                Objects.equals(description, project.description) &&
                user.equals(project.user) &&
                creationTime.equals(project.creationTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, ocrWcRatingThreshold, verificationsPerWord, user, creationTime);
    }

    @Override
    public String toString() {
        return "Project{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", ocrWcRatingThreshold=" + ocrWcRatingThreshold +
                ", verificationsPerWord=" + verificationsPerWord +
                ", user=" + user +
                ", creationTime=" + creationTime +
                '}';
    }

    public static class Builder {

        private Long id;
        private String name;
        private String description;
        private Double ocrWcRatingThreshold;
        private Integer verificationsPerWord;
        private Instant creationTime;
        private User user;

        public Project.Builder setId(Long id) {
            validateId(id);
            this.id = id;
            return this;
        }

        public Project.Builder setName(String name) {
            validateName(name);
            this.name = name;
            return this;
        }

        public Project.Builder setDescription(String description) {
            validateDescription(description);
            this.description = description;
            return this;
        }

        public Project.Builder setOcrWcRatingThreshold(Double ocrWcRatingThreshold) {
            validateOcrWcRatingThreshold(ocrWcRatingThreshold);
            this.ocrWcRatingThreshold = ocrWcRatingThreshold;
            return this;
        }

        public Project.Builder setVerificationPerWord(Integer verificationsPerWord) {
            validateVerificationsPerWord(verificationsPerWord);
            this.verificationsPerWord = verificationsPerWord;
            return this;
        }

        public Project.Builder setUser(User user) {
            validateUser(user);
            this.user = user;
            return this;
        }

        public Project.Builder setCreationTime(Instant instant) {
            validateCreationTime(instant);
            this.creationTime = instant;
            return this;
        }

        public Project build() {
            if (name == null || ocrWcRatingThreshold == null || verificationsPerWord == null
                    || creationTime == null || user == null) {
                throw new IllegalArgumentException("All values must be set");
            }
            return new Project(this);
        }
    }
}