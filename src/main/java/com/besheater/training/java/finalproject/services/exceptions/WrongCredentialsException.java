package com.besheater.training.java.finalproject.services.exceptions;

public class WrongCredentialsException extends Exception{
    public WrongCredentialsException() {
        super();
    }

    public WrongCredentialsException(String s) {
        super(s);
    }

    public WrongCredentialsException(String message, Throwable cause) {
        super(message, cause);
    }

    public WrongCredentialsException(Throwable cause) {
        super(cause);
    }
}