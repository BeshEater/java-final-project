package com.besheater.training.java.finalproject.repositories;

import com.besheater.training.java.finalproject.entities.Project;
import com.besheater.training.java.finalproject.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProjectRepoJDBC implements ProjectRepo {
    private static final Logger LOG = LogManager.getLogger();

    private final DataSource dataSource;
    private final UserRepo userRepo;

    public ProjectRepoJDBC(DataSource dataSource, UserRepo userRepo) {
        this.dataSource = dataSource;
        this.userRepo = userRepo;
    }

    @Override
    public Optional<Project> save(Project project) {
        String query = "INSERT INTO projects(name, description, ocr_wc_rating_threshold, verifications_per_word, user_id, creation_time) " +
                "VALUES( ?, ?, ?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setDouble(3, project.getOcrWcRatingThreshold());
            statement.setInt(4, project.getVerificationsPerWord());
            statement.setLong(5, project.getUser().getId());
            statement.setTimestamp(6, Timestamp.from(project.getCreationTime()));
            statement.executeUpdate();
            Optional<Project> savedProject = findOneByName(project.getName());
            LOG.debug("Project {} saved", savedProject);
            return savedProject;

        } catch (SQLException ex) {
            LOG.error("Can't save project", ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Project> update(Long projectId, Project project) {
        return Optional.empty();
    }

    @Override
    public Optional<Project> findOneById(Long id) {
        String query = "SELECT * FROM projects WHERE id = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setLong(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            Project project = mapToProject(rs);
            return Optional.of(project);

        } catch (SQLException ex) {
            LOG.error("Can't find project by id", ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<Project> findOneByName(String name) {
        String query = "SELECT * FROM projects WHERE name = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            rs.next();
            Project project = mapToProject(rs);
            return Optional.of(project);

        } catch (SQLException ex) {
            LOG.error("Can't find project by name", ex);
        }
        return Optional.empty();
    }

    @Override
    public List<Project> findAllByUser(User user) {
        return null;
    }

    @Override
    public List<Project> getAll() {
        String query = "SELECT * FROM projects";

        List<Project> projects = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                projects.add(mapToProject(rs));
            }
        } catch (SQLException ex) {
            LOG.error("Can't get all projects", ex);
        }
        return projects;
    }

    @Override
    public boolean delete(Long id) {
        return false;
    }

    private Project mapToProject(ResultSet rs) throws SQLException {
        Project project = null;
        try {
            project = new Project.Builder()
                    .setId(rs.getLong("id"))
                    .setName(rs.getString("name"))
                    .setDescription(rs.getString("description"))
                    .setOcrWcRatingThreshold(rs.getDouble("ocr_wc_rating_threshold"))
                    .setVerificationPerWord(rs.getInt("verifications_per_word"))
                    .setUser(userRepo.findOneById(rs.getLong("user_id")).get())
                    .setCreationTime(rs.getTimestamp("creation_time").toInstant())
                    .build();
        } catch (IllegalArgumentException ex) {
            LOG.error("Can't recreate Project from ResultSet", ex);
        }
        return project;
    }
}