CREATE TABLE users(
  id IDENTITY PRIMARY KEY,
  username VARCHAR(20) NOT NULL UNIQUE,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL UNIQUE,
  password VARCHAR(100) NOT NULL,
  registration_time TIMESTAMP NOT NULL
);

CREATE TABLE projects(
  id IDENTITY PRIMARY KEY,
  name VARCHAR(50) NOT NULL UNIQUE ,
  description VARCHAR(500),
  ocr_wc_rating_threshold DOUBLE NOT NULL,
  verifications_per_word INT NOT NULL,
  user_id BIGINT NOT NULL,
  creation_time TIMESTAMP NOT NULL,
  CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users(id)
);