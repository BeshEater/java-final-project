package com.besheater.training.java.finalproject.repositories;

import com.besheater.training.java.finalproject.entities.Project;
import com.besheater.training.java.finalproject.entities.User;

import java.util.List;
import java.util.Optional;

public interface ProjectRepo {

    Optional<Project> save(Project project);
    Optional<Project> update(Long projectId, Project project);
    Optional<Project> findOneById(Long id);
    Optional<Project> findOneByName(String name);
    List<Project> findAllByUser(User user);
    List<Project> getAll();
    boolean delete(Long id);
}