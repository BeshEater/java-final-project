package com.besheater.training.java.finalproject.repositories;

import com.besheater.training.java.finalproject.entities.User;

import java.util.Optional;

public interface UserRepo {

    Optional<User> save(User user);
    Optional<User> update(Long userId, User user);
    Optional<User> findOneById(Long id);
    Optional<User> findOneByUsername(String username);
    Optional<User> findOneByEmail(String email);
    boolean delete(Long id);
}
