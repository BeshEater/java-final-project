package com.besheater.training.java.finalproject.filters;

import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

public class LocaleDispatcher implements Filter {
    private static final Logger LOG = LogManager.getLogger();

    private static final Locale DEFAULT_LOCALE = new Locale("en");
    private static final Map<String, Locale> SUPPORTED_LOCALES;

    static {
        Locale[] locales = {new Locale("en"),
                            new Locale("ru")};
        SUPPORTED_LOCALES = Arrays.stream(locales)
                .collect(Collectors.toMap(Locale::getLanguage, locale -> locale));
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Locale locale = extractLocale(request);
        if (locale == null) {
            // URL without locale info
            setLocale(request, DEFAULT_LOCALE);
            chain.doFilter(request, response);
        } else {
            setLocale(request, locale);
            request.getRequestDispatcher(extractServletPath(request)).forward(request, response);
        }
    }

    public static void redirect(HttpServletRequest req, HttpServletResponse resp, String path)
            throws IOException {
        String contextPath = req.getContextPath();
        Locale reqLocale = (Locale) req.getAttribute(Attributes.LOCALE);
        String language;
        if (reqLocale != null) {
            language = reqLocale.getLanguage();
        } else {
            language = extractLocale(req).getLanguage();
        }
        resp.sendRedirect(contextPath + "/" + language + path);
    }

    private void setLocale(HttpServletRequest request, Locale locale) {
        request.setAttribute(Attributes.LOCALE, locale);
        Config.set(request, Config.FMT_LOCALE, locale);
        LOG.debug("Set locale {}", locale);
    }

    public static Locale extractLocale(HttpServletRequest request) {
        String[] split = request.getServletPath().split("/");
        if (split.length > 1) {
            String language = split[1];
            LOG.debug("Language from URL = {}", language);
            return SUPPORTED_LOCALES.get(language);
        }
        return null;
    }

    public static String extractServletPath(HttpServletRequest request) {
        String path = request.getServletPath();
        int secondSlashIndex = path.indexOf("/", 1);
        String servletPath;
        if (secondSlashIndex > 0) {
            servletPath = path.substring(secondSlashIndex);
        } else {
            servletPath = "/";
        }
        LOG.debug("Servlet path from URL = {}", servletPath);
        return servletPath;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException { }

    @Override
    public void destroy() { }
}