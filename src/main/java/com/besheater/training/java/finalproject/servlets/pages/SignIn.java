package com.besheater.training.java.finalproject.servlets.pages;

import com.besheater.training.java.finalproject.entities.User;
import com.besheater.training.java.finalproject.filters.LocaleDispatcher;
import com.besheater.training.java.finalproject.services.DiContainer;
import com.besheater.training.java.finalproject.services.UserManager;
import com.besheater.training.java.finalproject.services.exceptions.UserNotExistException;
import com.besheater.training.java.finalproject.services.exceptions.WrongCredentialsException;
import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/sign-in")
public class SignIn extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger();

    private UserManager userManager;

    @Override
    public void init() throws ServletException {
        userManager = DiContainer.getInstance().getUserManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("GET request");
        setPageUrl(req);
        if (req.getParameter("url") != null) {
            req.setAttribute(Attributes.ERROR, "page.sign-in.error.authentication-required");
        }
        dispatchToView(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("POST request");
        setPageUrl(req);
        String username = req.getParameter("username");
        String rawPassword = req.getParameter("password");
        if (username != null && rawPassword != null) {
            try {
                User user = userManager.authenticate(username, rawPassword);
                req.getSession().setAttribute(Attributes.LOGGED_USER, user);
                String initialUrl = req.getParameter("url");
                if (initialUrl != null) {
                    LocaleDispatcher.redirect(req, resp, initialUrl);
                } else {
                    LocaleDispatcher.redirect(req, resp, "/");
                }
            } catch (WrongCredentialsException | UserNotExistException e) {
                LOG.error("Can't authenticate user", e);
                req.setAttribute(Attributes.ERROR, "page.sign-in.error.username-or-password-not-valid");
                dispatchToView(req, resp);
            }
        } else {
            req.setAttribute(Attributes.ERROR, "page.sign-in.error.username-or-password-not-valid");
            dispatchToView(req, resp);
        }
    }

    private void dispatchToView(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/view/sign-in.jsp").forward(req, resp);
    }

    private void setPageUrl(HttpServletRequest req) {
        String pageUrl = "/sign-in";
        String queryString = req.getQueryString();
        if (queryString != null) {
            pageUrl += "?" + queryString;
        }
        req.setAttribute(Attributes.PAGE_URL, pageUrl);
    }
}