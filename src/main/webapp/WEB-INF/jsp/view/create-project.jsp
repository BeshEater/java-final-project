<%@ page import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<template:main pageUrl="/create-project">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.create-project.title"/>
    </jsp:attribute>
    <jsp:attribute name="additionalScripts">
        <script>
            $.validator.localisedMessages = {
                project_name: {
                    required: "<fmt:message key='page.create-project.form.project-name.error'/>",
                    alphanumeric: "<fmt:message key='general.form.error.alphanumeric'/>",
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${1}</fmt:param><fmt:param>${50}</fmt:param></fmt:message>",
                },
                description: {
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${1}</fmt:param><fmt:param>${500}</fmt:param></fmt:message>",
                },
                ocr_wc_rating_threshold: {
                    required: "<fmt:message key='page.create-project.form.ocr-wc-rating-threshold.error'/>",
                    number: "<fmt:message key='general.form.error.number'/>",
                    range: "<fmt:message key='general.form.error.value-range'><fmt:param>${0}</fmt:param><fmt:param>${1}</fmt:param></fmt:message>",
                },
                verifications_per_word: {
                    required: "<fmt:message key='page.create-project.form.verifications-per-word.error'/>",
                    digits: "<fmt:message key='general.form.error.integer-number'/>",
                    range: "<fmt:message key='general.form.error.value-range'><fmt:param>${1}</fmt:param><fmt:param>${10}</fmt:param></fmt:message>",
                },
            };
        </script>
        <script src="<c:url value="/assets/js/create-project.js" />"></script>
    </jsp:attribute>
    <jsp:body>
        <main id="main">

            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.create-project.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.create-project.title"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <c:if test="${requestScope[Attributes.ERROR] != null}">
                                <div class="general-block error-block">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-2">
                                            <i class="icofont-close-circled"></i>
                                        </div>
                                        <div class="col-lg-10">
                                            <p><fmt:message key="${requestScope[Attributes.ERROR]}"/></p>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <form id="create-project-form" action="<util:localePrefixUrl value="/create-project" />" method="post" role="form" class="general-form">

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="project_name"><fmt:message key="page.create-project.form.project-name"/></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="project_name" name="project_name"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="description"><fmt:message key="page.create-project.form.description"/></label>
                                    <div class="col-lg-8">
                                        <textarea class="form-control" id="description" name="description" rows="5" ></textarea>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label" for="ocr_wc_rating_threshold"><fmt:message key="page.projects.ocr-wc-rating-threshold"/></label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="ocr_wc_rating_threshold" name="ocr_wc_rating_threshold"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-6 col-form-label" for="verifications_per_word"><fmt:message key="page.projects.verifications-per-word"/></label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" id="verifications_per_word" name="verifications_per_word" />
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit"><fmt:message key="page.create-project.title"/></button>
                                </div>

                            </form>
                        </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->
    </jsp:body>
</template:main>