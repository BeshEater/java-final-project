package com.besheater.training.java.finalproject.services;

import com.besheater.training.java.finalproject.entities.Project;
import com.besheater.training.java.finalproject.repositories.ProjectRepo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class ProjectManager {
    private static final Logger LOG = LogManager.getLogger();

    private final ProjectRepo projectRepo;

    public ProjectManager(ProjectRepo projectRepo) {
        this.projectRepo = projectRepo;
    }

    public Optional<Project> saveProject(Project project) {
        LOG.debug("Saving project: {}", project);
        return projectRepo.save(project);
    }

    public Optional<Project> updateProject(Long projectId, Project project) {
        LOG.debug("Updating project with id: {} to {}", projectId, project);
        return projectRepo.update(projectId, project);
    }

    public List<Project> getAllProjects() {
        LOG.debug("Getting all projects");
        return projectRepo.getAll();
    }

    public boolean deleteProject(Project project) {
        LOG.debug("Deleting project: {}", project);
        Long id = project.getId();
        if (id == null) {
            throw new IllegalArgumentException("Project id cannot be null");
        }
        return projectRepo.delete(id);
    }
}