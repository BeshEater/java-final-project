<template:main pageUrl="/correction-task">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.correction-task.title"/>
    </jsp:attribute>
    <jsp:body>
        <main id="main">
            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.correction-task.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.correction-task.title"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->
        </main><!-- End #main -->
    </jsp:body>
</template:main>
