<%@ page import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<template:main pageUrl="/projects">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.projects.title"/>
    </jsp:attribute>
    <jsp:body>
        <main id="main">
            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.projects.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.projects.title"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->

            <!-- ======= Projects Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="projects-controls text-center">
                                <a href="<util:localePrefixUrl value="/create-project" />"><fmt:message key="page.projects.create-new-project"/></a>
                            </div>
                            <c:forEach items="${requestScope[Attributes.PROJECTS]}" var="project">
                                <div class="project-box">
                                    <h3>${project.name}</h3>
                                    <p>${project.description}</p>
                                    <div class="project-settings">
                                        <div class="row">
                                            <p class="col-lg-6"><fmt:message key="page.projects.ocr-wc-rating-threshold"/></p>
                                            <p class="col-lg-1">${fn:escapeXml(project.ocrWcRatingThreshold)}</p>
                                        </div>
                                        <div class="row">
                                            <p class="col-lg-6"><fmt:message key="page.projects.verifications-per-word"/></p>
                                            <p class="col-lg-1">${fn:escapeXml(project.verificationsPerWord)}</p>
                                        </div>
                                        <div class="row">
                                            <p class="col-lg-6"><fmt:message key="page.projects.author"/></p>
                                            <a href="<util:localePrefixUrl value="/user?username=${fn:escapeXml(project.user.username)}" />" class="col-lg-6">${fn:escapeXml(project.user.username)}</a>
                                        </div>
                                        <div class="row">
                                            <p class="col-lg-6"><fmt:message key="page.projects.creation-date"/></p>
                                            <p class="col-lg-6"><javatime:format value="${project.creationTime}" style="MS" /></p>
                                        </div>
                                        <div class="project-controls-block">
                                            <a href="#"><fmt:message key="page.projects.change"/></a>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->
    </jsp:body>
</template:main>