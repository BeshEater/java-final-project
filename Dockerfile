FROM maven:3.6-adoptopenjdk-11 AS builder
WORKDIR /app
# Make a new layer with all required Maven dependencies in order to use native Docker cashing
COPY pom.xml .
RUN ["mvn", "dependency:go-offline", "verify", "--fail-never"]
# Package main artifact
COPY . .
RUN ["mvn", "clean", "package"]

FROM tomcat:9.0.37-jdk11-adoptopenjdk-hotspot
COPY --from=builder /app/target/*.war $CATALINA_HOME/webapps/ROOT.war