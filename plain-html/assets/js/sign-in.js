 $.validator.setDefaults( {
        submitHandler: function () {
            alert( "submitted!" );
        }
    } );

$( document ).ready( function () {
    $( "#sign-in-form" ).click(function() {
        $(".error-block").fadeOut();
    });

    $( "#sign-in-form" ).validate( {
        rules: {
            email: "required",
            password: "required",
        },
        messages: {
            email: "Please enter an email",
            password: "Please provide a password",
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "form-validation-hint-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        }
    } );
} );