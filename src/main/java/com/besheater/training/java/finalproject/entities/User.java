package com.besheater.training.java.finalproject.entities;

import com.besheater.training.java.finalproject.services.UserManager;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.Objects;
import java.util.regex.Pattern;

public class User {
    private static final Logger LOG = LogManager.getLogger();

    private static final String FIRST_NAME_REGEX = "[\\w-]{1,50}";
    private static final String LAST_NAME_REGEX = "[\\w-]{1,100}";
    private static final String USERNAME_REGEX = "[\\w-]{3,20}";
    private static final int MIN_PASSWORD_LENGTH = 6;
    private static final int MAX_PASSWORD_LENGTH = 100;
    private static final int MIN_ENCODED_PASSWORD_LENGTH = 20;

    private final Long id;
    private final String firstName;
    private final String lastName;
    private final String username;
    private final String email;
    private final String encodedPassword;
    private final Instant registrationTime;

    private User(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.username = builder.username;
        this.email = builder.email;
        this.registrationTime = builder.registrationTime;
        if (builder.rawPassword != null) {
            this.encodedPassword = UserManager.encodePassword(builder.rawPassword);
        } else {
            this.encodedPassword = builder.encodedPassword;
        }
    }

    private static void validateId(Long id) {
        if(id == null || id < 0) {
            throw new IllegalArgumentException("Id: " + id + " is not valid");
        }
    }

    private static void validateFirstName(String firstName) {
        if(firstName == null || !isMatch(firstName, FIRST_NAME_REGEX)) {
            throw new IllegalArgumentException("First name: " + firstName + " is not valid");
        }
    }

    private static void validateLastName(String lastName) {
        if (lastName == null || !isMatch(lastName, LAST_NAME_REGEX)) {
            throw new IllegalArgumentException("Last name: " + lastName + " is not valid");
        }
    }

    private static void validateUsername(String username) {
        if (username == null || !isMatch(username, USERNAME_REGEX)) {
            throw new IllegalArgumentException("Username: " + username + " is not valid");
        }
    }

    private static void validateEmail(String email) {
        if (email == null || !EmailValidator.getInstance().isValid(email)) {
            throw new IllegalArgumentException("Email: " + email + " is not valid");
        }
    }

    private static void validateRawPassword(String rawPassword) {
        if (rawPassword == null || rawPassword.length() < MIN_PASSWORD_LENGTH
                                || rawPassword.length() > MAX_PASSWORD_LENGTH) {
            throw new IllegalArgumentException("Password: " + rawPassword + " is not valid");
        }
    }

    private static void validateEncodedPassword(String encodedPassword) {
        if (encodedPassword == null || encodedPassword.length() < MIN_ENCODED_PASSWORD_LENGTH) {
            throw new IllegalArgumentException("Encoded password: " + encodedPassword + " is not valid");
        }
    }

    private static void validateRegistrationTime(Instant instant) {
        if (instant == null) {
            throw new IllegalArgumentException("Registration time: " + instant + " is not valid");
        }
    }

    private static boolean isMatch(CharSequence input, String regex) {
        Pattern p = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        return p.matcher(input).matches();
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public Instant getRegistrationTime() {
        return registrationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                firstName.equals(user.firstName) &&
                lastName.equals(user.lastName) &&
                username.equals(user.username) &&
                email.equals(user.email) &&
                encodedPassword.equals(user.encodedPassword) &&
                registrationTime.equals(user.registrationTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, username, email, encodedPassword, registrationTime);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", encodedPassword='" + encodedPassword + '\'' +
                ", registrationTime=" + registrationTime +
                '}';
    }

    public static class Builder {
        private Long id;
        private String firstName;
        private String lastName;
        private String username;
        private String email;
        private String rawPassword;
        private String encodedPassword;
        private Instant registrationTime;

        public Builder setId(Long id) {
            validateId(id);
            this.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            validateFirstName(firstName);
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            validateLastName(lastName);
            this.lastName = lastName;
            return this;
        }

        public Builder setUsername(String username) {
            validateUsername(username);
            this.username = username;
            return this;
        }

        public Builder setEmail(String email) {
            validateEmail(email);
            this.email = email;
            return this;
        }

        public Builder setRawPassword(String rawPassword) {
            validateRawPassword(rawPassword);
            if (encodedPassword != null) {
                throw new IllegalArgumentException("Can't set both raw and encoded password");
            }
            this.rawPassword = rawPassword;
            return this;
        }

        public Builder setEncodedPassword(String encodedPassword) {
            validateEncodedPassword(encodedPassword);
            if (rawPassword != null) {
                throw new IllegalArgumentException("Can't set both raw and encoded password");
            }
            this.encodedPassword = encodedPassword;
            return this;
        }

        public Builder setRegistrationTime(Instant instant) {
            validateRegistrationTime(instant);
            this.registrationTime = instant;
            return this;
        }

        public User build() {
            if (firstName == null || lastName == null || username == null ||
                    email == null || registrationTime == null ||
                    (rawPassword == null && encodedPassword == null)) {
                throw new IllegalArgumentException("All values must be set");
            }
            return new User(this);
        }
    }
}