<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ tag import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<%@ attribute name="value" type="java.lang.String" rtexprvalue="true" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:url value='${"/"}${requestScope[Attributes.LOCALE].language}${value}' />