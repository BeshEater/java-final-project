<template:main pageUrl="/error">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.error.title"/>
    </jsp:attribute>
    <jsp:body>
        <main id="main">
            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.error.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.error.internal-server-error"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-6">
                            <div class="general-block error-block">
                                <div class="row justify-content-center">
                                    <div class="col-lg-2">
                                        <i class="icofont-close-circled"></i>
                                    </div>
                                    <div class="col-lg-10">
                                        <p><fmt:message key="page.error.internal-server-error.text"/></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->
    </jsp:body>
</template:main>