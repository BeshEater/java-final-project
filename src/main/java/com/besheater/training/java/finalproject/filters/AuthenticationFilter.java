package com.besheater.training.java.finalproject.filters;

import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class AuthenticationFilter implements Filter {
    private static final Logger LOG = LogManager.getLogger();

    private List<String> restrictedPaths;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String restrictedPathsString = filterConfig.getInitParameter("restrictedPaths");
        restrictedPaths = Arrays.stream(restrictedPathsString
                                 .split("\n"))
                                 .map(String::trim)
                                 .collect(Collectors.toList());
        LOG.debug("Restricted paths {}", restrictedPaths);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String servletPath = LocaleDispatcher.extractServletPath(req);
        if (isRequiredAuthentication(servletPath)) {
            if ( req.getSession().getAttribute(Attributes.LOGGED_USER) == null) {
                LocaleDispatcher.redirect(req, resp, "/sign-in?url=" + servletPath);
                LOG.debug("Authentication required for path {}", servletPath);
            } else {
                chain.doFilter(request, response);
                LOG.debug("Authenticated user accessed restricted path {}", servletPath);
            }
        } else {
            chain.doFilter(request, response);
            LOG.debug("Authentication for path {} is not required", servletPath);
        }
    }

    private boolean isRequiredAuthentication(String servletPath) {
        for (String restrictedPath : restrictedPaths) {
            if (servletPath.startsWith(restrictedPath)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void destroy() { }
}