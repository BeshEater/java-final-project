<%@ page import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<template:main pageUrl="/account">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.account.title"/>
    </jsp:attribute>
    <jsp:attribute name="additionalScripts">
        <script>
            $.validator.localisedMessages = {
                first_name: {
                    required: "<fmt:message key='page.sign-up.form.first-name.error'/>",
                    alphanumeric: "<fmt:message key='general.form.error.alphanumeric'/>",
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${1}</fmt:param><fmt:param>${50}</fmt:param></fmt:message>",
                },
                last_name: {
                    required: "<fmt:message key='page.sign-up.form.last-name.error'/>",
                    alphanumeric: "<fmt:message key='general.form.error.alphanumeric'/>",
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${1}</fmt:param><fmt:param>${100}</fmt:param></fmt:message>",
                },
                username: {
                    required: "<fmt:message key='page.sign-up.form.username.error'/>",
                    alphanumeric: "<fmt:message key='general.form.error.alphanumeric'/>",
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${3}</fmt:param><fmt:param>${20}</fmt:param></fmt:message>",
                },
                password: {
                    required: "<fmt:message key='page.sign-up.form.password.error'/>",
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${6}</fmt:param><fmt:param>${100}</fmt:param></fmt:message>",
                },
                confirm_password: {
                    required: "<fmt:message key='page.sign-up.form.confirm-password.error'/>",
                    rangelength: "<fmt:message key='general.form.error.length-range'><fmt:param>${6}</fmt:param><fmt:param>${100}</fmt:param></fmt:message>",
                    equalTo: "<fmt:message key='page.sign-up.form.confirm-password.error.equal-to'/>",
                },
                email: "<fmt:message key='page.sign-up.form.email.error'/>",
            };
        </script>
        <script src="<c:url value="/assets/js/sign-up.js" />"></script>
    </jsp:attribute>
    <jsp:body>
        <main id="main">

            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.account.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.account.title"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-7">
                            <c:if test="${requestScope[Attributes.ERROR] != null}">
                                <div class="general-block error-block">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-2">
                                            <i class="icofont-close-circled"></i>
                                        </div>
                                        <div class="col-lg-10">
                                            <p><fmt:message key="${requestScope[Attributes.ERROR]}"/></p>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <form id="sign-up-form" action="<util:localePrefixUrl value="/account" />" method="post" role="form" class="general-form">
                                <c:set var = "user" value = "${sessionScope[Attributes.LOGGED_USER]}"/>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="first_name"><fmt:message key="page.sign-up.form.first-name"/></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="first_name" name="first_name" value="${fn:escapeXml(user.firstName)}" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="last_name"><fmt:message key="page.sign-up.form.last-name"/></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="last_name" name="last_name" value="${fn:escapeXml(user.lastName)}" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="username"><fmt:message key="page.sign-up.form.username"/></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="username" name="username" value="${fn:escapeXml(user.username)}" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="email"><fmt:message key="page.sign-up.form.email"/></label>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="email" name="email" value="${fn:escapeXml(user.email)}" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="password"><fmt:message key="page.sign-up.form.password"/></label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="<fmt:message key="page.account.form.new-password"/>" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-lg-4 col-form-label" for="confirm_password"><fmt:message key="page.sign-up.form.confirm-password"/></label>
                                    <div class="col-lg-8">
                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="<fmt:message key="page.account.form.confirm-new-password"/>" />
                                    </div>
                                </div>

                                <div class="text-center">
                                    <button type="submit"><fmt:message key="page.account.form.save-changes"/></button>
                                </div>

                            </form>
                        </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->
    </jsp:body>
</template:main>