$.validator.addMethod( "alphanumeric", function( value, element ) {
    return this.optional( element ) || XRegExp('^[\\p{L}\\p{N}_-]+$').test( value );
}, "Letters, numbers, hyphens and underscores only please" );