package com.besheater.training.java.finalproject.servlets.pages;

import com.besheater.training.java.finalproject.services.DiContainer;
import com.besheater.training.java.finalproject.services.UserManager;
import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/user")
public class User extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger();

    private UserManager userManager;

    @Override
    public void init() throws ServletException {
        userManager = DiContainer.getInstance().getUserManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("Get request");
        setPageUrl(req);
        String username = req.getParameter("username");
        Optional<com.besheater.training.java.finalproject.entities.User> userOptional =
                userManager.getUserByUsername(username);
        if (userOptional.isPresent()) {
            req.setAttribute(Attributes.USER, userOptional.get());
        } else {
            req.setAttribute(Attributes.ERROR, "error");
        }
        req.getRequestDispatcher("/WEB-INF/jsp/view/user.jsp").forward(req, resp);
    }

    private void setPageUrl(HttpServletRequest req) {
        String pageUrl = "/user";
        String queryString = req.getQueryString();
        if (queryString != null) {
            pageUrl += "?" + queryString;
        }
        req.setAttribute(Attributes.PAGE_URL, pageUrl);
    }
}