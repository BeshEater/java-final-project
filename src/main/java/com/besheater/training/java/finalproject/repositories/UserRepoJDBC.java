package com.besheater.training.java.finalproject.repositories;

import com.besheater.training.java.finalproject.entities.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

public class UserRepoJDBC implements UserRepo {
    private static final Logger LOG = LogManager.getLogger();

    private final DataSource dataSource;

    public UserRepoJDBC(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<User> save(User user) {
        String query = "INSERT INTO users(username, first_name, last_name, email, password, registration_time) " +
                "VALUES( ?, ?, ?, ?, ?, ?)";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getEncodedPassword());
            statement.setTimestamp(6, Timestamp.from(user.getRegistrationTime()));
            statement.executeUpdate();
            Optional<User> savedUser = findOneByUsername(user.getUsername());
            LOG.debug("User {} added", savedUser);
            return savedUser;

        } catch (SQLException ex) {
            LOG.error("Can't save user", ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> update(Long userId, User user) {
        String query = "UPDATE users SET username = ?, first_name = ?, last_name = ?, " +
                       "email = ?, password =?, registration_time = ? " +
                       "WHERE id = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, user.getUsername());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getLastName());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getEncodedPassword());
            statement.setTimestamp(6, Timestamp.from(user.getRegistrationTime()));
            statement.setLong(7, userId);
            statement.executeUpdate();
            Optional<User> updatedUser = findOneById(userId);
            LOG.debug("User {} saved", updatedUser);
            return updatedUser;

        } catch (SQLException ex) {
            LOG.error("Can't save user", ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findOneById(Long userId) {
        String query = "SELECT * FROM users WHERE id = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setLong(1, userId);
            ResultSet rs = statement.executeQuery();
            rs.next();
            User user = mapToUser(rs);
            return Optional.of(user);

        } catch (SQLException ex) {
            LOG.error("Can't find user by id", ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findOneByUsername(String username) {
        String query = "SELECT * FROM users WHERE username = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, username);
            ResultSet rs = statement.executeQuery();
            rs.next();
            User user = mapToUser(rs);
            return Optional.of(user);

        } catch (SQLException ex) {
            LOG.error("Can't find user by username", ex);
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> findOneByEmail(String email) {
        String query = "SELECT * FROM users WHERE email = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            rs.next();
            User user = mapToUser(rs);
            return Optional.of(user);

        } catch (SQLException ex) {
            LOG.error("Can't find user by email", ex);
        }
        return Optional.empty();
    }

    @Override
    public boolean delete(Long id) {
        String query = "DELETE FROM users WHERE id = ?";

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setLong(1, id);
            int updateCount = statement.executeUpdate();
            return updateCount > 0;

        } catch (SQLException ex) {
            LOG.error("Can't delete user by id", ex);
        }
        return false;
    }

    private User mapToUser(ResultSet rs) throws SQLException {
        return new User.Builder()
                .setId(rs.getLong("id"))
                .setUsername(rs.getString("username"))
                .setFirstName(rs.getString("first_name"))
                .setLastName(rs.getString("last_name"))
                .setEmail(rs.getString("email"))
                .setEncodedPassword(rs.getString("password"))
                .setRegistrationTime(rs.getTimestamp("registration_time").toInstant())
                .build();
    }
}