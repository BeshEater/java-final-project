package com.besheater.training.java.finalproject.servlets.pages;

import com.besheater.training.java.finalproject.entities.User;
import com.besheater.training.java.finalproject.filters.LocaleDispatcher;
import com.besheater.training.java.finalproject.services.DiContainer;
import com.besheater.training.java.finalproject.services.UserManager;
import com.besheater.training.java.finalproject.services.exceptions.UserAlreadyExistException;
import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

@WebServlet("/sign-up")
public class SignUp extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger();

    private UserManager userManager;

    @Override
    public void init() throws ServletException {
        userManager = DiContainer.getInstance().getUserManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("GET request");
        dispatchToView(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("POST request");
        String firstName = req.getParameter("first_name");
        String lastName = req.getParameter("last_name");
        String username = req.getParameter("username");
        String email = req.getParameter("email");
        String rawPassword = req.getParameter("password");
        String rawPasswordConfirm = req.getParameter("confirm_password");

        if (firstName != null && lastName != null && username != null &&
                email != null && rawPassword != null && rawPasswordConfirm != null) {
            if (!rawPassword.equals(rawPasswordConfirm)) {
                LOG.error("Passwords aren't equals");
                req.setAttribute(Attributes.ERROR, "page.sign-up.error.confirm-password-is-not-the-same");
                dispatchToView(req, resp);
            } else {
                try {
                    User user = new User.Builder()
                                        .setFirstName(firstName)
                                        .setLastName(lastName)
                                        .setUsername(username)
                                        .setEmail(email)
                                        .setRawPassword(rawPassword)
                                        .setRegistrationTime(Instant.now())
                                        .build();
                    Optional<User> registeredUser = userManager.registerUser(user);
                    if (registeredUser.isPresent()) {
                        req.getSession().setAttribute(Attributes.LOGGED_USER, registeredUser.get());
                        LocaleDispatcher.redirect(req, resp, "/");
                        LOG.debug("New user successfully registered {}", registeredUser.get());
                    } else {
                        LOG.error("Can't register user {}", user);
                        req.setAttribute(Attributes.ERROR, "general.form.error.data-is-not-valid");
                        dispatchToView(req, resp);
                    }
                } catch (IllegalArgumentException e) {
                    LOG.error("User is not valid", e);
                    req.setAttribute(Attributes.ERROR, "general.form.error.data-is-not-valid");
                    dispatchToView(req, resp);
                } catch (UserAlreadyExistException e) {
                    LOG.error("User already exist", e);
                    req.setAttribute(Attributes.ERROR, "page.sign-up.error.user-already-exist");
                    dispatchToView(req, resp);
                }
            }
        } else {
            LOG.error("Some fields are null");
            req.setAttribute(Attributes.ERROR, "general.form.error.data-is-not-valid");
            dispatchToView(req, resp);
        }
    }

    private void dispatchToView(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/jsp/view/sign-up.jsp").forward(req, resp);
    }
}