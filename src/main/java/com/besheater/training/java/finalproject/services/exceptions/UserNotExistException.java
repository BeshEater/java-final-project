package com.besheater.training.java.finalproject.services.exceptions;

public class UserNotExistException extends Exception{
    public UserNotExistException() {
        super();
    }

    public UserNotExistException(String s) {
        super(s);
    }

    public UserNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotExistException(Throwable cause) {
        super(cause);
    }
}