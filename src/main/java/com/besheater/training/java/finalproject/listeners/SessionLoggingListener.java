package com.besheater.training.java.finalproject.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionIdListener;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionLoggingListener implements HttpSessionListener, HttpSessionIdListener {
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        LOG.info("Session {} created", se.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        LOG.info("Session {} destroyed", se.getSession().getId());
    }

    @Override
    public void sessionIdChanged(HttpSessionEvent event, String oldSessionId) {
        LOG.info("Session ID {} changed to {}", oldSessionId, event.getSession().getId());
    }
}
