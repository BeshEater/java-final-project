<template:main pageUrl="/">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.index.title"/>
    </jsp:attribute>
    <jsp:body>
        <!-- ======= Hero Section ======= -->
        <section id="hero">
            <div class="hero-container">
                <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">

                    <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                    <div class="carousel-inner" role="listbox">

                        <!-- Slide 1 -->
                        <div class="carousel-item active" style="background: url(<c:url value="/assets/img/slide/slide-1.jpg" />)">
                            <div class="carousel-container">
                                <div class="carousel-content">
                                    <h2 class="animated fadeInDown"><fmt:message key="page.index.headline.welcome"/><span> <fmt:message key="page.index.headline.welcome.name"/></span></h2>
                                    <p class="animated fadeInUp"><fmt:message key="page.index.headline.text"/></p>
                                    <a href="#" class="btn-get-started animated fadeInUp"><fmt:message key="page.index.headline.more"/></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>

                    <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>
            </div>
        </section><!-- End Hero -->

        <main id="main">

            <!-- ======= Featured Section ======= -->
            <section id="featured" class="featured">
                <div class="container">

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="icon-box">
                                <i class="icofont-papers"></i>
                                <h3><a href="<util:localePrefixUrl value="/projects" />"><fmt:message key="page.index.featured.projects"/></a></h3>
                                <p><fmt:message key="page.index.featured.projects.text"/></p>
                            </div>
                        </div>
                        <div class="col-lg-4 mt-4 mt-lg-0">
                            <div class="icon-box">
                                <i class="icofont-spreadsheet"></i>
                                <h3><a href="<util:localePrefixUrl value="/statistics" />"><fmt:message key="page.index.featured.statistics"/></a></h3>
                                <p><fmt:message key="page.index.featured.statistics.text"/></p>
                            </div>
                        </div>
                        <div class="col-lg-4 mt-4 mt-lg-0">
                            <div class="icon-box">
                                <i class="icofont-read-book"></i>
                                <h3><a href="<util:localePrefixUrl value="/about" />"><fmt:message key="page.index.featured.about"/></a></h3>
                                <p><fmt:message key="page.index.featured.about.text"/></p>
                            </div>
                        </div>
                    </div>

                </div>
            </section><!-- End Featured Section -->
        </main><!-- End #main -->
    </jsp:body>
</template:main>