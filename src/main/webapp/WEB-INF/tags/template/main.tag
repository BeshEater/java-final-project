<%@ tag body-content="scriptless" trimDirectiveWhitespaces="true" %>
<%@ tag import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<%@ attribute name="htmlTitle" type="java.lang.String" rtexprvalue="true" required="true" %>
<%@ attribute name="pageUrl" type="java.lang.String" rtexprvalue="true" required="true" %>
<%@ attribute name="headContent" fragment="true" required="false" %>
<%@ attribute name="bodyTopContent" fragment="true" required="false" %>
<%@ attribute name="additionalScripts" fragment="true" required="false" %>
<%@ include file="/WEB-INF/jsp/base.jspf" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title><c:out value="${fn:trim(htmlTitle)}" /></title>
        <meta content="" name="descriptison">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="<c:url value="/assets/img/favicon.png" />" rel="icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Inter:300,300i,400,400i,500,500i,600,600i,700,700i|Rubik:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="<c:url value="/assets/vendor/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
        <link href="<c:url value="/assets/vendor/icofont/icofont.min.css" />" rel="stylesheet">
        <link href="<c:url value="/assets/vendor/boxicons/css/boxicons.min.css" />" rel="stylesheet">
        <link href="<c:url value="/assets/vendor/animate.css/animate.min.css" />" rel="stylesheet">
        <link href="<c:url value="/assets/vendor/owl.carousel/assets/owl.carousel.min.css" />" rel="stylesheet">
        <link href="<c:url value="/assets/vendor/venobox/venobox.css" />" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="<c:url value="/assets/css/style.css" />" rel="stylesheet">

        <!-- Additional head content-->
        <jsp:invoke fragment="headContent" />

        <!-- =======================================================
        * Template Name: Eterna - v2.0.0
        * Template URL: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/
        * Author: BootstrapMade.com
        * License: https://bootstrapmade.com/license/
        ======================================================== -->
    </head>

    <body>
    <jsp:invoke fragment="bodyTopContent" />

    <!-- ======= Top Bar ======= -->
    <section id="topbar" class="d-none d-lg-block">
        <div class="container d-flex">
            <div class="contact-info mr-auto">
            </div>
            <div class="social-links">
                <p><fmt:message key="page.index.top-bar.change-language"/></p>
                <a href="<c:url value="/ru${pageUrl}" />" ><fmt:message key="page.index.top-bar.change-language.ru"/></a>
                <a href="<c:url value="/en${pageUrl}" />" ><fmt:message key="page.index.top-bar.change-language.en"/></a>
            </div>
        </div>
    </section>

    <!-- ======= Header ======= -->
    <header id="header">
        <div class="container d-flex">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="<util:localePrefixUrl value="/" />"><span>TCorrector</span></a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                    <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                    <li><a href="<util:localePrefixUrl value="/projects" />"><fmt:message key="template.header.projects"/></a></li>
                    <li class="drop-down"><a href="#"><fmt:message key="template.header.quick-task"/></a>
                        <ul>
                            <li><a href="<util:localePrefixUrl value="/verification-task" />"><fmt:message key="template.header.verification"/></a></li>
                            <li><a href="<util:localePrefixUrl value="/correction-task" />"><fmt:message key="template.header.correction"/></a></li>
                        </ul>
                    </li>
                    <li><a href="<util:localePrefixUrl value="/statistics" />"><fmt:message key="template.header.statistics"/></a></li>
                    <li><a href="<util:localePrefixUrl value="/about" />"><fmt:message key="template.header.about"/></a></li>
                    <li>
                        <c:choose>
                            <c:when test="${sessionScope[Attributes.LOGGED_USER] != null}">
                                <div class="logged-user">
                                    <a href="<util:localePrefixUrl value="/account" />">${fn:escapeXml(sessionScope[Attributes.LOGGED_USER].username)}</a>
                                    <a href="<util:localePrefixUrl value="/sign-out" />"><i class="icofont-exit"></i></a>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <a class="sign-in" href="<util:localePrefixUrl value="/sign-in" />"><fmt:message key="template.header.sign-in"/>
                            </c:otherwise>
                        </c:choose>
                        </a>
                    </li>

                </ul>
            </nav><!-- .nav-menu -->

        </div>
    </header><!-- End Header -->

    <!-- Main page content -->
    <jsp:doBody />

    <!-- ======= Footer ======= -->
    <footer id="footer">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4><fmt:message key="template.footer.useful-links"/></h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.useful-links.home"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.useful-links.about"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.useful-links.projects"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.useful-links.terms"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.useful-links.privacy-policy"/></a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4><fmt:message key="template.footer.my-account"/></h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.my-account.sign-in"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.my-account.registration"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.my-account.account"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.my-account.stats"/></a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#"><fmt:message key="template.footer.my-account.feedback"/></a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4><fmt:message key="template.footer.contact-us"/></h4>
                        <p>

                            <strong><fmt:message key="template.footer.contact-us.telegram"/></strong> <fmt:message key="template.footer.contact-us.telegram.name"/><br>
                            <strong><fmt:message key="template.footer.contact-us.email"/></strong> <fmt:message key="template.footer.contact-us.email.address"/><br>
                        </p>

                    </div>

                    <div class="col-lg-3 col-md-6 footer-info">
                        <h3><fmt:message key="template.footer.about"/></h3>
                        <p><fmt:message key="template.footer.about.text"/></p>
                        <div class="social-links mt-3">
                            <p><fmt:message key="template.footer.language"/></p>
                            <a href="<c:url value="/ru${pageUrl}" />"><fmt:message key="template.footer.language.ru"/></a>
                            <a href="<c:url value="/en${pageUrl}" />"><fmt:message key="template.footer.language.en"/></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                <fmt:message key="template.footer.copyright"/><strong><span> <fmt:message key="template.footer.copyright.author"/></span></strong><fmt:message key="template.footer.copyright.all-right-reserved"/>
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/ -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
            </div>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

    <!-- Vendor JS Files -->
    <script src="<c:url value="/assets/vendor/jquery/jquery.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/jquery.easing/jquery.easing.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/php-email-form/validate.js" />"></script>
    <script src="<c:url value="/assets/vendor/jquery-sticky/jquery.sticky.js" />"></script>
    <script src="<c:url value="/assets/vendor/owl.carousel/owl.carousel.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/waypoints/jquery.waypoints.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/counterup/counterup.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/isotope-layout/isotope.pkgd.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/venobox/venobox.min.js" />"></script>
    <script src="<c:url value="/assets/vendor/xregexp/xregexp-all.js" />"></script>
    <script src="<c:url value="/assets/vendor/jquery-validation/jquery.validate.js" />"></script>
    <script src="<c:url value="/assets/vendor/jquery-validation/alphanumeric.js" />"></script>

    <!-- Template Main JS File -->
    <script src="<c:url value="/assets/js/main.js" />"></script>

    <!-- Additional footer content -->
    <jsp:invoke fragment="additionalScripts" />

    </body>
</html>