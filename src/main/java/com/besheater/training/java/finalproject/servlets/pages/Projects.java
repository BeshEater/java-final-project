package com.besheater.training.java.finalproject.servlets.pages;

import com.besheater.training.java.finalproject.entities.Project;
import com.besheater.training.java.finalproject.services.DiContainer;
import com.besheater.training.java.finalproject.services.ProjectManager;
import com.besheater.training.java.finalproject.servlets.Attributes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/projects")
public class Projects extends HttpServlet {
    private static final Logger LOG = LogManager.getLogger();

    private ProjectManager projectManager;

    @Override
    public void init() throws ServletException {
        projectManager = DiContainer.getInstance().getProjectManager();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.debug("Get request");
        List<Project> projects = projectManager.getAllProjects();
        req.setAttribute(Attributes.PROJECTS, projects);
        req.getRequestDispatcher("/WEB-INF/jsp/view/projects.jsp").forward(req, resp);
    }
}