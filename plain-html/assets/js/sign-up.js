 $.validator.setDefaults( {
        submitHandler: function () {
            alert( "submitted!" );
        }
    } );

$( document ).ready( function () {
    $( "#sign-up-form" ).click(function() {
        $(".error-block").fadeOut();
    });

    $( "#sign-up-form" ).validate( {
        rules: {
            firstname: {
                required: true,
                alphanumeric: true,
                rangelength: [1, 50],
            },
            lastname: {
                required: true,
                alphanumeric: true,
                rangelength: [1, 100],
            },
            username: {
                required: true,
                alphanumeric: true,
                rangelength: [3, 20],
            },
            password: {
                required: true,
                rangelength: [6, 100],
            },
            confirm_password: {
                required: true,
                rangelength: [6, 100],
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            firstname: {
                required: "Please enter your first name",
                alphanumeric: "Must consist of only letters, numbers, hyphens and underscores",
                rangelength: "Length must be between 1 and 50 characters",
            },
            lastname: {
                required: "Please enter your last name",
                alphanumeric: "Must consist of only letters, numbers, hyphens and underscores",
                rangelength: "Length must be between 1 and 100 characters",
            },
            username: {
                required: "Please enter a username",
                alphanumeric: "Must consist of only letters, numbers, hyphens and underscores",
                rangelength: "Length must be between 3 and 20 characters",
            },
            password: {
                required: "Please provide a password",
                rangelength: "Length must be between 6 and 100 characters",
            },
            confirm_password: {
                required: "Please provide a password",
                rangelength: "Length must be between 6 and 100 characters",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
        },
        errorElement: "em",
        errorPlacement: function ( error, element ) {
            // Add the `help-block` class to the error element
            error.addClass( "form-validation-hint-block" );

            if ( element.prop( "type" ) === "checkbox" ) {
                error.insertAfter( element.parent( "label" ) );
            } else {
                error.insertAfter( element );
            }
        },
        highlight: function ( element, errorClass, validClass ) {
            $( element ).addClass( "is-invalid" ).removeClass( "is-valid" );
        },
        unhighlight: function (element, errorClass, validClass) {
            $( element ).addClass( "is-valid" ).removeClass( "is-invalid" );
        }
    } );
} );