<%@ page import="com.besheater.training.java.finalproject.servlets.Attributes"%>
<template:main pageUrl="${requestScope[Attributes.PAGE_URL]}">
    <jsp:attribute name="htmlTitle">
        <fmt:message key="page.sign-in.title"/>
    </jsp:attribute>
    <jsp:attribute name="additionalScripts">
         <script>
             $.validator.localisedMessages = {
                 username: "<fmt:message key='page.sign-in.form.username.error'/>",
                 password: "<fmt:message key='page.sign-in.form.password.error'/>",
             };
         </script>
        <script src="<c:url value="/assets/js/sign-in.js" />"></script>
    </jsp:attribute>
    <jsp:body>
        <main id="main">

            <!-- ======= Breadcrumbs ======= -->
            <section id="breadcrumbs" class="breadcrumbs">
                <div class="container">

                    <ol>
                        <li><a href="<util:localePrefixUrl value="/" />"><fmt:message key="template.header.home"/></a></li>
                        <li><fmt:message key="page.sign-in.title"/></li>
                    </ol>
                    <h2><fmt:message key="page.sign-in.title"/></h2>

                </div>
            </section><!-- End Breadcrumbs -->

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-4">
                            <c:if test="${requestScope[Attributes.ERROR] != null}">
                                <div class="general-block error-block">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-2">
                                            <i class="icofont-close-circled"></i>
                                        </div>
                                        <div class="col-lg-10">
                                            <p><fmt:message key="${requestScope[Attributes.ERROR]}"/></p>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                            <form id="sign-in-form" action="<util:localePrefixUrl value="${requestScope[Attributes.PAGE_URL]}" />" method="post" role="form" class="general-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="username" name="username" placeholder="<fmt:message key="page.sign-in.form.username"/>" />
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" placeholder="<fmt:message key="page.sign-in.form.password"/>" />
                                </div>
                                <div class="text-center">
                                    <button type="submit"><fmt:message key="page.sign-in.form.sign-in"/></button>
                                </div>
                                <div class="text-center">
                                    <a class="create-an-account" href="<util:localePrefixUrl value="/sign-up" />"><fmt:message key="page.sign-in.form.create-an-account"/></a>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->

    </jsp:body>
</template:main>