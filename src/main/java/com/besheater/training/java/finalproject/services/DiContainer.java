package com.besheater.training.java.finalproject.services;

import com.besheater.training.java.finalproject.repositories.ProjectRepo;
import com.besheater.training.java.finalproject.repositories.ProjectRepoJDBC;
import com.besheater.training.java.finalproject.repositories.UserRepo;
import com.besheater.training.java.finalproject.repositories.UserRepoJDBC;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

/**
 *  Manual Di container. Just satisfy all dependencies by hand, what could possibly go wrong?
 */
public class DiContainer {
    private DataSource dataSource;
    private UserRepo userRepo;
    private ProjectRepo projectRepo;
    private UserManager userManager;
    private ProjectManager projectManager;

    private DiContainer() {
        init();
    }

    public static DiContainer getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        public static final DiContainer INSTANCE = new DiContainer();
    }

    private void init() {
        dataSource = getHikariDataSource();
        userRepo = new UserRepoJDBC(dataSource);
        projectRepo = new ProjectRepoJDBC(dataSource, userRepo);
        userManager = new UserManager(userRepo);
        projectManager = new ProjectManager(projectRepo);
    }

    private DataSource getHikariDataSource() {
        HikariConfig config = new HikariConfig("/hikari.properties");
        return new HikariDataSource(config);
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public UserRepo getUserRepo() {
        return userRepo;
    }

    public ProjectRepo getProjectRepo() {
        return projectRepo;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public ProjectManager getProjectManager() {
        return projectManager;
    }
}